##############################################
MET_RefStand- meteorological reference stand
##############################################
:Author: Greg Cohn
:Date: 2016-04-08

.. image:: https://bitbucket.org/repo/azK6kK/images/2544655760-newtlogo5.gif

This repository houses CRBasic programs to operate CR1000_ 's. The programs log temperature data at remote reference
stands at the HJ Andrews_ Experimental Forest.

******************************
Download Current Programs
******************************
* RS_v03.113_
    * Deployed to CS2MET on 5-20-19
    * logger programs_03.113_
    * Source code_03.113_
        * Prerequisites - Python 2.7.1
    * commit af7d3ec_
* RS_v03.0_
	* Deployed to all stations 6-22-16
	* logger programs_3.0_
	* source code_3.0_
		* Prerequisites - Python 2.7.1
	* commit 53dbec4_
* RS_v2.5_
    * Deployed to RS05 on 4-13-16
    * logger programs_2.5_
    * Source code_2.5_
        * Prerequisites - Python 2.7.1
    * commit d158dc5_
* RS_v2.0_
    * Released 04-06-16
    * logger programs_2.0_
    * Source code_2.0_
        * Prerequisites - Python 2.7.1
    * commit  9fc1a76_
* Latest development_
    * `Source code`_

Release Notes
==============

V03.113 updates
--------------
V03.113 initializes check values as NAN instead of 0. CS2MET (Climatic Station 2) was added. This station has no soil
sensors, but has a hobo, and AIR_03 thermistor. The AIR_02 thermistor is replaced by AIR_150 HC2S3 Temp/RH sensor. The
name is a legacy Alias.


V03.0 updates
--------------
V03.0 adds check values to the data table. These new fields are set as NAN values except when manually changed in the
Public Table. This allows sensor checks done during a site visit to be included in the data stream with an accurate
timestamp.

V02.5 updates
--------------
Reference stand 5 was formatted for thermocouples, but had thermistors at the site. This change **only impacts RS5** and
**only the temperature call has changed**.

V02 updates
------------
V02 creates an automated build process to make all reference stand programs from the same program template. This
ensures identical methods on all loggers. For each reference stand, the template is copied, and file names and logger
id's are edited using the same convention as V01.

Min/Max
^^^^^^^^^
The CR1000_ 's don't maintain a daily table. Calculating a max/min from 5 min averages is not backwards compatible
with daily max/min. To fix this a max/min measurement was added.

Soil Thermistors
^^^^^^^^^^^^^^^^^
Original Edlog programs broke the reference stand into 3 groups:
* soil temp measured with thermistors (RSMISTERS)
* soil temp measured with thermocouples (RSCOUPLES)
* RH measurements present
V01 only worked with thermocouples. V02 adds soil thermistors.

Known Issues
^^^^^^^^^^^^^
V02 does not handle HMPC45C RH measurements

******************
Installations
******************
All data used to be collected by a CR10_ or a CR10x_ which use the Edlog_ programming language. These systems had 1
temperature probe, and soil temperature measurements at 3 soil depths. No Edlog code is handled here.

Upgrades
===================
In 2014 sites began receiving upgrades to the meteorological sensors. CR1000_ were added with 2 new temperature sensors
as well as a HOBO_ pendant in a gill aspirated shield. The latest version upgrades the programming to use one unified
code where python copies a template into a new directory with the appropriate headers, logger id's, and file names.

Current Configuration
======================
+----------------+------+--------------+----------------+-------------------+
|Reference Stand |LID   |Upgrade Date  |Program Version |Soil Measurement   |
+================+======+==============+================+===================+
|2               |090   +4-9-14        +RS_V03.0        +Thermocouple       |
+----------------+------+--------------+----------------+-------------------+
|4               |091   +5-29-14       +RS_V03.0        +Thermocouple       |
+----------------+------+--------------+----------------+-------------------+
|5               |092   +4-13-16       +RS_V03.0        +Thermistor         |
+----------------+------+--------------+----------------+-------------------+
|10              |093   +4-6-16        +RS_V03.0        +Thermistor         |
+----------------+------+--------------+----------------+-------------------+
|12              |094   +5-7-14        +RS_V03.0        +Thermocouple       |
+----------------+------+--------------+----------------+-------------------+
|20              |095   +4-14-14       +RS_V03.0        +Thermocouple       |
+----------------+------+--------------+----------------+-------------------+
|26              |096   +4-30-14       +RS_V03.0        +Thermocouple       |
+----------------+------+--------------+----------------+-------------------+
|38              |038   +5-09-16       +RS_V03.0        +Thermistor         |
+----------------+------+--------------+----------------+-------------------+
|86              |086   +6-06-16       +RS_V03.0        +Thermistor         |
+----------------+------+--------------+----------------+-------------------+
|89              |089   +6-01-16       +RS_V03.0        +Thermistor         |
+----------------+------+--------------+----------------+-------------------+
|CLIM 113/CS2MET |113   |5-20-19       |RS_V03.113      |None + RH          |
+----------------+------+--------------+----------------+-------------------+


*****************************
This Repository
*****************************
The repository stores documentation as well as the tools to generate code for all of the reference stands using an
automated build process. To simply **download the latest programs** installed on loggers in the field, download this
version_.

Version Conventions
====================
**Version numbers** change when any new developments or upgrades are added.

**Sub-versions** change when an alteration is made to accommodate a single stand, but does not impact the overall
program. Sub-version numbers match the stand that the changes apply to.

:Example:

     Reference stand 5 was switched from a thermocouple to a thermistor. This new version was v02.5.

     * version in program: ``PROG_VERS = 2.5``
     * Program name: RS05_092_v02.5.cr1
     * Version tag in repository: RS05_v02.5

Downloading the repository
===========================


Direct Download
----------------
You can directly download the repository by going to **Downloads** from the projects' BitBucket page.

* Go to **Downloads**
* click on the **Tags** tab.
* Download the .zip file for the version you want.

For the latest development version:

* click on the **Branches** tab.
* click on the **develop** branch.
* download the .zip file

Using GIT
------------------
GIT is a versioning software with many nice GUI's available (e.g. SourceTree_, GitKraken_ GitHubDesktop_ ). Install your
favorite version of Git on your machine.

.. attention::

    Mac users must either use SourceTree_ or use git_ in terminal

In your GUI or command line:

1. **clone** the repository to your local machine
2. pull the either the ``default`` or ``development`` branch

.. code::

    git clone https://USERNAME@bitbucket.org/hjandrews/met_refstand
    git pull && git update DESIREDbranchNAME

Using the Program
==================
This program requires that Python 2.x be in the system path, or that a virtual environment be used. It is recommended
that you install anaconda on your machine and create a conda environment using the conda_environment.yaml that is
provided. Details below about both methods.

Using conda
------------
This is the recommended method. In DOS (shorthand for PowerShell or command prompt), navigate to the directory with
:code:`make.bat` and enter the following:

.. code-block:: bat

    >conda env create -f conda_environment.yaml
    >conda activate refstands

Your screen should now display:

.. code-block:: bat

    (refstands)>

To run the program and then exit the python environment (which will only run python 2.x):

.. code-block:: bat

    (refstand)>make.bat
    (refstand)>conda deactivate
    >


Double clicking (must add Python 2.x to system path)
------------------------------------------------------
Since Python 3.x is now the default, you must make sure that Python 2.x is listed before any other python
installations. This will permanently make python 2.x the default on your machine:

+ Right click on **My Computer** and select *Properties*
+ Select *Advanced System Settings*
+ On the **Advanced** tab, select *Environmental Variables*
+ Select *PATH* and click *Edit*
+ Place a semicolon at the end of the list and add the file path to the python install

.. code::

    %SystemRoot%;C:\python-2.7.5.amd64\Scripts\python.exe


To build the logger programs for each reference stand, double click on :code:`make.bat`.


Double click w/o making Python 2.x your system default
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

+ Do exactly as above, but change the name of :code:`python.exe` to :code:`python2.exe`.
+ Open :code:`make.bat` and change the word :code`python` to :code:`python2`

Dependencies
-------------
All dependencies are built in to Python; no additional installations should be necessary. However, use of a conda
environment is recommended, and a suitable conda_environment.yaml is provided. Note, the program was designed for
Python 2.x, which is handled by the environment file.
* Python 2.7.1
    * json
    * re (RegExp)
    * shutil - make_archive, move
    * os - getcwd
    * datetime

Contributing to Development
----------------------------
The repository contains 3 files:

1. RS.CR1- this file contains the program that will be used in all reference stands
2. RefStand.lid- this is a text file which contains a JSON of metadata about each reference stand
3. make.bat- this batch file can be executed on any windows machine

Changes to the source code should be done on the development_ branch.

.. _Andrews : http://andrewsforest.oregonstate.edu/
.. _CR1000 : https://www.campbellsci.com/cr1000

.. _RS_v03.113 :  https://bitbucket.org/hjandrews/met_refstands/commits/tag/RS_v03.113
.. _programs_03.113 : https://bitbucket.org/hjandrews/met_refstands/downloads/CLIM113_V03.113.cr1
.. _code_03.113 : https://bitbucket.org/hjandrews/met_refstands/get/RS_v03.113.zip
.. _af7d3ec : https://bitbucket.org/hjandrews/met_refstands/commits/af7d3ec0d94e0ac88ce2d59f8be3c2fa49981b7f

.. _RS_v03.0 :  https://bitbucket.org/hjandrews/met_refstands/commits/tag/RS_v03.0
.. _programs_3.0 : https://bitbucket.org/hjandrews/met_refstands/downloads/RS_v3.0.zip
.. _code_3.0 : https://bitbucket.org/hjandrews/met_refstands/get/RS_v03.0.zip
.. _53dbec4 : https://bitbucket.org/hjandrews/met_refstands/commits/53dbec46b4f9d4e552e9a324c6d81b9e8a081931

.. _RS_v2.5 : https://bitbucket.org/hjandrews/met_refstands/commits/tag/RS05_V02.5
.. _programs_2.5: https://bitbucket.org/hjandrews/met_refstands/downloads/RS5_V02.5.cr1
.. _code_2.5: https://bitbucket.org/hjandrews/met_refstands/get/RS05_V02.5.zip
.. _d158dc5:  https://bitbucket.org/hjandrews/met_refstands/commits/d158dc59a9e6217cf11d99548825d1931c1109e4

.. _RS_v2.0 : https://bitbucket.org/hjandrews/met_refstands/commits/tag/RS_v02.0
.. _programs_2.0: https://bitbucket.org/hjandrews/met_refstands/downloads/RS_v2.0.zip
.. _code_2.0: https://bitbucket.org/hjandrews/met_refstands/get/RS_v02.0.zip
.. _9fc1a76:  https://bitbucket.org/hjandrews/met_refstands/commits/9fc1a7620467ec7ef1916dccc53270af8037042e

.. _development: https://bitbucket.org/hjandrews/met_refstands/branch/develop#commits
.. _Source code: https://bitbucket.org/hjandrews/met_refstands/get/develop.zip

.. _CR10 : https://www.campbellsci.com/cr10
.. _CR10x : https://www.campbellsci.com/cr10x
.. _Edlog : https://www.campbellsci.com/blog/edlog-program-file

.. _HOBO : http://www.testequipmentdepot.com/onset/data-loggers/multi-parameter-measurements/indoor-outdoor-environments/hobo-pendant-temp-light-data-logger-ua00264.htm

.. _version : https://bitbucket.org/hjandrews/met_refstand/downloads/RS_v2.0.zip

.. _SourceTree: https://www.atlassian.com/software/sourcetree
.. _GitHubDesktop:
.. _GitKraken: https://www.gitkraken.com/download
.. _git: https://git-scm.com/downloads

.. _PythonXY: http://python-xy.github.io/downloads.html
.. _WinPython: https://winpython.github.io/
