"""
build.py
===========
Creates programs for Campbell Scientific CR1000 data loggers to monitor under-canopy temperature at reference stands in
the HJ Andrews Experimental Forest. Output directory contains CrBasic files.

This maintains consistent naming conventions and uniformity between programs. It also allows updates and changes to
be applied universally, eliminating diversion between programs at different sites

What the module does
--------------------

A single standard CRbasic program, RS.cr1, is loaded into memory. Output filename, logger ID, and type of soil and
air sensor is customized to each logger ID, and an output directory, ./bin, is created with a unique .cr1 for each
site. All files created are then archived in a single .zip folder.

Inputs needed
-------------
CRbasic file RS.cr1
^^^^^^^^^^^^^^^^^^^

This file is written so that it is clear and understandable independent of this python module. Changes or additions
should follow CRBasic conventions.

To use this program without running this python module simply:
 #. enter a value for line 10
 #. Choose a soil sensor by commenting line 84 or 85

    .. literalinclude:: ../../RS.cr1
      :language: basic
      :lines: 9-12
      :lineno-start: 9
      :emphasize-lines: 2

    .. literalinclude:: ../../RS.cr1
      :language: basic
      :lines: 84-85
      :lineno-start: 84
      :emphasize-lines: 1-

#. Choose either:

    #. HC2S3 temp/rh sensor: comment line 26 and 75
    #. Redundant Temp107: comment 27, 76-77, and all lines with 'RH' or 'DEW' (21, 27, 33-34, 40-41, 57-59, 76-81)

    .. literalinclude:: ../../RS.cr1
      :language: basic
      :lines: 26-28
      :lineno-start: 26
      :emphasize-lines: 1-2

    .. literalinclude:: ../../RS.cr1
      :language: basic
      :lines: 75-81
      :lineno-start: 75
      :emphasize-lines: 1

rs_config.yaml
^^^^^^^^^^^^^^
A yaml file, rs_config.yaml, contains metadata about each station. A file is created for each site in this list, so
sites can easily be added or removed. Format required at V03.113

:Example:

.. code-block:: yaml

    2:
        LID: 090
        SOIL: thermocouple
        PREFIX: RS

Soil Options
""""""""""""

* thermocouple
* thermistor
* None

PREFIX
""""""
Accommodates stations that are not a part of the reference stand network, but are small under-canopy stations
using a CR1000 with redundant temp sensors.

RH options
""""""""""""

Additional options were added to include a temp/RH probe. If `RH: HC2S3` is not including rs_config.yaml, then all
RH related sensors are removed from the program. When RH is included, it replaces the temp107 thermistor `AIR(1)`,
`Alias AIR_02`. RH sensors have integrated temp sensors. At the moment, all RH sensors use a `VoltDiff()` call.

"""
__authors__ = 'Greg Cohn'
__version__ = '1.0'

import yaml
import re
from datetime import datetime
from shutil import make_archive, copy
from os import getcwd, remove
from os.path import join


class BuildRsProg:
    """
    Build CrBasic programs for under canopy temperature monitoring using a CR1000 data logger at long term vegetation
    Reference Stands.

    This class reads a generic .cr1 file and customizes it as specified by a .yaml.
    """

    def __init__(self):

        self.lid_list = {}
        self.prog_str = ''
        self.prog_vers = 0.0
        self.prog_lid_index = ()

    def read_rs_config(self, file_n='./rs_config.yaml'):
        """
        Read YAML file rs_config.yaml. This file contains basic information about each logger, it's sensors, and it's
        wiring.

        :param file_n: string containing filename or path
        :return: dictionary assigned to class object
        """

        with open(file_n) as f:
            lid_list = yaml.safe_load(f)

        self.lid_list = lid_list

    def read_prog(self, file='./RS.CR1'):
        """
        Read .cr1 file as one str.

        Regular expressions are used to customize the file once read.

        :param file: str. File name
        :return: str assigned to class object
        """
        with open(file) as f:
            lines = f.read()

        self.prog_str = lines

    def find_prog_vers(self):
        """
        Search the program to determine the program version and save to class object.
        """
        prog_vers = float(re.findall("PROG_VERS[^\n]*", self.prog_str)[0].split("=")[-1])

        self.prog_vers = prog_vers

    def find_lid_index(self):
        """
        Search the program to index where LID is defined and save int to class object.
        """
        m = re.search("LID[^\n]*", self.prog_str)
        self.prog_lid_index = m.span()

    def get_lid(self, rs_num):
        """
        Return the logger ID for a site from the loaded config.yaml.

        :param rs_num: int. Key to dictionary of reference stand configurations.
        :return: int. Logger ID number
        """
        return self.lid_list[rs_num]['LID']

    def get_prefix(self, rs_num):
        """
        Return the project prefix for a site from the loaded config.yaml.

        :Example:
            'CLIM' is prefix for CS2MET
            'RS' is prefix for reference stands

        :param rs_num: int. Key to dictionary of reference stand configurations.
        :return: str. Project prefix.
        """
        return self.lid_list[rs_num]['PREFIX']

    def get_prog_file_name(self, rs_num):
        """
        Return a customized file name for a site's program compiled from configuration information.

        :param rs_num: int. Key to dictionary of reference stand configurations.
        :return: str. Customized filename.
        """
        lid = self.get_lid(rs_num)
        lid = '' if int(lid) == rs_num else '_' + lid
        prefix = self.get_prefix(rs_num)
        vers = str(self.prog_vers)
        vers = f'0{vers}' if vers.split('.')[0].__len__() < 2 else vers

        return f'{prefix}{rs_num}{lid}_V{vers}.cr1'

    def get_usb_file_name(self, rs_num):
        """
        Return a customized name for USB download file compiled from configuration information.

        :param rs_num: int. Key to dictionary of reference stand configurations.
        :return: str. Customized download filename.
        """
        lid = self.get_lid(rs_num)
        lid = '' if int(lid) == rs_num else f'_{lid}'
        prefix = self.get_prefix(rs_num)

        return f"{prefix}{rs_num}{lid}_Table105"

    def get_header(self, fname):
        """
        Get text to stamp output program headers with autogen information.

        :param fname: str. Name of file to be output.
        :return: str. Header text.
        """
        header = f"' Program name: {fname}"
        header += f"\n' Auto generated by build.py on {datetime.now().strftime('%m-%d-%y %H:%M')}\n"

        return header

    def get_soil_method(self, rs_num):
        """
        Return information on which soil sensor is used for a site from the loaded config.yaml.

        :param rs_num: int. Key to dictionary of reference stand configurations.
        :return: str. Soil sensor used.
        """
        return self.lid_list[rs_num]['SOIL']

    def get_has_rh(self, rs_num):
        """
        Determine if site is configured with an RH sensor from the loaded config.yaml.

        :param rs_num: int. Key to dictionary of reference stand configurations.
        :return: boolean
        """
        return 'RH' in self.lid_list[rs_num]

    @staticmethod
    def _del_lines(search_strs, txt, append_pattern=''):
        "search_strs is a list of strs to search for. A pattern can optionally be appended to each str"

        new_txt = txt
        for s in search_strs:
            new_txt = re.sub("(.*?)" + s + append_pattern + "(.*?)[\n]", "", new_txt)

        return new_txt

    def format_RH_method(self, rs_num, txt='self'):
        """
        Format the program's RH method based on the site configuration.

        If the site does not have RH, all RH measurements and alternate aliases for AIR(1) are deleted. If site does
        have RH, standard aliases of AIR(1) are deleted.

        :param rs_num: int. Key to dictionary of reference stand configurations.
        :param txt: keyword str. Allows a str other than the class object's program to be formatted.
        :return: str. New customized program str.
        """
        if txt == 'self':
            txt = self.prog_str
        has_rh = self.get_has_rh(rs_num)

        if has_rh:
            del_str = ['AIR_02', 'AIR_TEMP,2']
        elif not has_rh:
            del_str = ['RH', 'HC2S3', '_150']

        return self._del_lines(del_str, txt, append_pattern='')

    def format_soil_method(self, rs_num, txt='self'):
        """
        Format the program's soil measurement method based on the site configuration.

        Available methods:
            * thermocouple (deletes thermistor)
            * thermistor (deletes thermocouple)
            * none (deletes all soil)

        :param rs_num: int. Key to dictionary of reference stand configurations.
        :param txt: keyword str. Allows a str other than the class object's program to be formatted.
        :return: str. New customized program str.
        """
        if txt == 'self':
            txt = self.prog_str
        soil_meth = self.get_soil_method(rs_num)

        soil_call = ['Therm107\(', 'TCDiff\(', "[^\n]*"]
        if soil_meth == 'thermocouple':
            del_str = [soil_call[0]]
        elif soil_meth == 'thermistor':
            del_str = [soil_call[1]]
        elif soil_meth == 'None':
             del_str = soil_call

        return self._del_lines(del_str, txt, append_pattern='SOIL')

    def save_prog(self, txt, fname=None, dir_n='./bin/'):
        """
        Saves program str to a file.

        :param txt: str. Program to write to file.
        :param fname: str. Name for new file.
        :param dir_n: str. Parent directory to save file in.
        """
        if fname is None:
            fname = self.get_prog_file_name()

        with open(join(dir_n, fname), 'w') as f:
            f.write(txt)

    def build_rs_prog(self, rs_num):
        """
        Build a customized program for a site based on its configuration and save to an output directory.

        :param rs_num: int. Key to dictionary of reference stand configurations.
        """
        text = self.prog_str

        lid = self.get_lid(rs_num)
        fname = self.get_prog_file_name(rs_num)
        usb = self.get_usb_file_name(rs_num)
        header = self.get_header(fname)
        fpath = './bin/'

        new_text = self.format_soil_method(rs_num, text)
        new_text = self.format_RH_method(rs_num, new_text)
        new_text = new_text.replace('LID =', f'LID = {lid}')
        new_text = new_text.replace('USB:', f'USB:{usb}')
        new_text = header + new_text
        self.save_prog(new_text, fname, dir_n=fpath)

        print(f'Successful write to {fpath}{fname}\n')

    def build_all_prog(self):
        """
        Build a customized program for each site in configuration file.
        """
        self.read_rs_config()
        self.read_prog()
        self.find_prog_vers()
        rs = self.lid_list

        for k in rs:
            self.build_rs_prog(k)

        print('-------------------------------------\nAll reference stands sucessfully written to ./bin/')

    def zip_all_prog(self):
        """
        Zip all programs created into a single archive.
        """
        fpath = getcwd()
        name = join(fpath, f'RS_v{self.prog_vers}')
        make_archive(name, 'zip', fpath, 'bin')
        copy(f'{name}.zip', './bin')
        remove(f'{name}.zip')

if __name__ == "__main__":
    programs = BuildRsProg()
    programs.build_all_prog()
    programs.zip_all_prog()
