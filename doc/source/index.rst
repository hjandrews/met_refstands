.. met_refstands documentation master file, created by
   sphinx-quickstart on Fri Oct 18 18:15:50 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

##########################################
Welcome to met_refstands's documentation!
##########################################

.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   includeREADME
   modules


##################
Indices and tables
##################

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
